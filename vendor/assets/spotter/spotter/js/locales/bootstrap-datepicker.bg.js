/**
 * Bulgarian translation for bootstrap-datepicker
 * Apostol Apostolov <apostol.s.apostolov@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['bg'] = {
		days: ["ÐÐµÐ´ÐµÐ»Ñ", "ÐÐ¾Ð½ÐµÐ´ÐµÐ»Ð½Ð¸Ðº", "ÐÑÐ¾ÑÐ½Ð¸Ðº", "Ð¡ÑÑÐ´Ð°", "Ð§ÐµÑÐ²ÑÑÑÑÐº", "ÐÐµÑÑÐº", "Ð¡ÑÐ±Ð¾ÑÐ°", "ÐÐµÐ´ÐµÐ»Ñ"],
		daysShort: ["ÐÐµÐ´", "ÐÐ¾Ð½", "ÐÑÐ¾", "Ð¡ÑÑ", "Ð§ÐµÑ", "ÐÐµÑ", "Ð¡ÑÐ±", "ÐÐµÐ´"],
		daysMin: ["Ð", "Ð", "Ð", "Ð¡", "Ð§", "Ð", "Ð¡", "Ð"],
		months: ["Ð¯Ð½ÑÐ°ÑÐ¸", "Ð¤ÐµÐ²ÑÑÐ°ÑÐ¸", "ÐÐ°ÑÑ", "ÐÐ¿ÑÐ¸Ð»", "ÐÐ°Ð¹", "Ð®Ð½Ð¸", "Ð®Ð»Ð¸", "ÐÐ²Ð³ÑÑÑ", "Ð¡ÐµÐ¿ÑÐµÐ¼Ð²ÑÐ¸", "ÐÐºÑÐ¾Ð¼Ð²ÑÐ¸", "ÐÐ¾ÐµÐ¼Ð²ÑÐ¸", "ÐÐµÐºÐµÐ¼Ð²ÑÐ¸"],
		monthsShort: ["Ð¯Ð½", "Ð¤ÐµÐ²", "ÐÐ°Ñ", "ÐÐ¿Ñ", "ÐÐ°Ð¹", "Ð®Ð½Ð¸", "Ð®Ð»Ð¸", "ÐÐ²Ð³", "Ð¡ÐµÐ¿", "ÐÐºÑ", "ÐÐ¾Ðµ", "ÐÐµÐº"],
		today: "Ð´Ð½ÐµÑ"
	};
}(jQuery));
