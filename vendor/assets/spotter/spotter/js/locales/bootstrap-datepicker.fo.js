/**
 * Faroese translation for bootstrap-datepicker
 * Theodor Johannesen <http://github.com/theodorjohannesen>
 */
;(function($){
	$.fn.datepicker.dates['fo'] = {
		days: ["Sunnudagur", "MÃ¡nadagur", "TÃ½sdagur", "Mikudagur", "HÃ³sdagur", "FrÃ­ggjadagur", "Leygardagur", "Sunnudagur"],
		daysShort: ["Sun", "MÃ¡n", "TÃ½s", "Mik", "HÃ³s", "FrÃ­", "Ley", "Sun"],
		daysMin: ["Su", "MÃ¡", "TÃ½", "Mi", "HÃ³", "Fr", "Le", "Su"],
		months: ["Januar", "Februar", "Marts", "AprÃ­l", "Mei", "Juni", "Juli", "August", "Septembur", "Oktobur", "Novembur", "Desembur"],
		monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
		today: "Ã Dag",
		clear: "Reinsa"
	};
}(jQuery));
