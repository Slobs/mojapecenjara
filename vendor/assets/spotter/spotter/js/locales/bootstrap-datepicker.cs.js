/**
 * Czech translation for bootstrap-datepicker
 * MatÄj KoubÃ­k <matej@koubik.name>
 * Fixes by Michal RemiÅ¡ <michal.remis@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['cs'] = {
		days: ["NedÄle", "PondÄlÃ­", "ÃterÃ½", "StÅeda", "Ätvrtek", "PÃ¡tek", "Sobota", "NedÄle"],
		daysShort: ["Ned", "Pon", "Ãte", "StÅ", "Ätv", "PÃ¡t", "Sob", "Ned"],
		daysMin: ["Ne", "Po", "Ãt", "St", "Ät", "PÃ¡", "So", "Ne"],
		months: ["Leden", "Ãnor", "BÅezen", "Duben", "KvÄten", "Äerven", "Äervenec", "Srpen", "ZÃ¡ÅÃ­", "ÅÃ­jen", "Listopad", "Prosinec"],
		monthsShort: ["Led", "Ãno", "BÅe", "Dub", "KvÄ", "Äer", "Änc", "Srp", "ZÃ¡Å", "ÅÃ­j", "Lis", "Pro"],
		today: "Dnes",
		clear: "Vymazat",
		weekStart: 1,
		format: "d.m.yyyy"
	};
}(jQuery));
