/**
 * Icelandic translation for bootstrap-datepicker
 * Hinrik Ãrn SigurÃ°sson <hinrik.sig@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['is'] = {
		days: ["Sunnudagur", "MÃ¡nudagur", "ÃriÃ°judagur", "MiÃ°vikudagur", "Fimmtudagur", "FÃ¶studagur", "Laugardagur", "Sunnudagur"],
		daysShort: ["Sun", "MÃ¡n", "Ãri", "MiÃ°", "Fim", "FÃ¶s", "Lau", "Sun"],
		daysMin: ["Su", "MÃ¡", "Ãr", "Mi", "Fi", "FÃ¶", "La", "Su"],
		months: ["JanÃºar", "FebrÃºar", "Mars", "AprÃ­l", "MaÃ­", "JÃºnÃ­", "JÃºlÃ­", "ÃgÃºst", "September", "OktÃ³ber", "NÃ³vember", "Desember"],
		monthsShort: ["Jan", "Feb", "Mar", "Apr", "MaÃ­", "JÃºn", "JÃºl", "ÃgÃº", "Sep", "Okt", "NÃ³v", "Des"],
		today: "Ã Dag"
	};
}(jQuery));
