/**
 * Persian translation for bootstrap-datepicker
 * Mostafa Rokooie <mostafa.rokooie@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['fa'] = {
		days: ["ÛÚ©âØ´ÙØ¨Ù", "Ø¯ÙØ´ÙØ¨Ù", "Ø³ÙâØ´ÙØ¨Ù", "ÚÙØ§Ø±Ø´ÙØ¨Ù", "Ù¾ÙØ¬âØ´ÙØ¨Ù", "Ø¬ÙØ¹Ù", "Ø´ÙØ¨Ù", "ÛÚ©âØ´ÙØ¨Ù"],
		daysShort: ["ÛÚ©", "Ø¯Ù", "Ø³Ù", "ÚÙØ§Ø±", "Ù¾ÙØ¬", "Ø¬ÙØ¹Ù", "Ø´ÙØ¨Ù", "ÛÚ©"],
		daysMin: ["Û", "Ø¯", "Ø³", "Ú", "Ù¾", "Ø¬", "Ø´", "Û"],
		months: ["ÚØ§ÙÙÛÙ", "ÙÙØ±ÛÙ", "ÙØ§Ø±Ø³", "Ø¢ÙØ±ÛÙ", "ÙÙ", "ÚÙØ¦Ù", "ÚÙØ¦ÛÙ", "Ø§ÙØª", "Ø³Ù¾ØªØ§ÙØ¨Ø±", "Ø§Ú©ØªØ¨Ø±", "ÙÙØ§ÙØ¨Ø±", "Ø¯Ø³Ø§ÙØ¨Ø±"],
		monthsShort: ["ÚØ§Ù", "ÙÙØ±", "ÙØ§Ø±", "Ø¢ÙØ±", "ÙÙ", "ÚÙÙ", "ÚÙÛ", "Ø§ÙØª", "Ø³Ù¾Øª", "Ø§Ú©Øª", "ÙÙØ§", "Ø¯Ø³Ø§"],
		today: "Ø§ÙØ±ÙØ²",
		clear: "Ù¾Ø§Ú© Ú©Ù",
		weekStart: 1,
		format: "yyyy/mm/dd"
	};
}(jQuery));
