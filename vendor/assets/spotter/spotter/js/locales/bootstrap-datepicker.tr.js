/**
 * Turkish translation for bootstrap-datepicker
 * Serkan Algur <kaisercrazy_2@hotmail.com>
 */
;(function($){
	$.fn.datepicker.dates['tr'] = {
		days: ["Pazar", "Pazartesi", "SalÄ±", "ÃarÅamba", "PerÅembe", "Cuma", "Cumartesi", "Pazar"],
		daysShort: ["Pz", "Pzt", "Sal", "ÃrÅ", "PrÅ", "Cu", "Cts", "Pz"],
		daysMin: ["Pz", "Pzt", "Sa", "Ãr", "Pr", "Cu", "Ct", "Pz"],
		months: ["Ocak", "Åubat", "Mart", "Nisan", "MayÄ±s", "Haziran", "Temmuz", "AÄustos", "EylÃ¼l", "Ekim", "KasÄ±m", "AralÄ±k"],
		monthsShort: ["Oca", "Åub", "Mar", "Nis", "May", "Haz", "Tem", "AÄu", "Eyl", "Eki", "Kas", "Ara"],
		today: "BugÃ¼n",
		clear: "Temizle",
		weekStart: 1,
		format: "dd.mm.yyyy"
	};
}(jQuery));

