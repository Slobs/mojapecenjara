// Azerbaijani
;(function($){
    $.fn.datepicker.dates['az'] = {
        days: ["Bazar", "Bazar ertÉsi", "ÃÉrÅÉnbÉ axÅamÄ±", "ÃÉrÅÉnbÉ", "CÃ¼mÉ axÅamÄ±", "CÃ¼mÉ", "ÅÉnbÉ", "Bazar"],
        daysShort: ["B.", "B.e", "Ã.a", "Ã.", "C.a", "C.", "Å.", "B."],
        daysMin: ["B.", "B.e", "Ã.a", "Ã.", "C.a", "C.", "Å.", "B."],
        months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Ä°yun", "Ä°yul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
        monthsShort: ["Yan", "Fev", "Mar", "Apr", "May", "Ä°yun", "Ä°yul", "Avq", "Sen", "Okt", "Noy", "Dek"],
        today: "Bu gÃ¼n",
        weekStart: 1
    };
}(jQuery));
